# [2.0.0](https://gitlab.com/AlexanderFSP/skb-semantic-release-experiments/compare/v1.1.0...v2.0.0) (2022-03-02)


### Features

* greet throws err instead of logging\ ([518ad06](https://gitlab.com/AlexanderFSP/skb-semantic-release-experiments/commit/518ad06e429fe8b4ac83e849a8fbc40eed6ec637))


### BREAKING CHANGES

* Surprise!

# [1.1.0](https://gitlab.com/AlexanderFSP/skb-semantic-release-experiments/compare/v1.0.1...v1.1.0) (2022-03-02)


### Features

* extend greet fn with optional name param ([546ff1e](https://gitlab.com/AlexanderFSP/skb-semantic-release-experiments/commit/546ff1ed61d11cb1a5a365bbb84649778a76a986))

## [1.0.1](https://gitlab.com/AlexanderFSP/skb-semantic-release-experiments/compare/v1.0.0...v1.0.1) (2022-03-02)


### Bug Fixes

* swap point with exclamation mark ([4d42e48](https://gitlab.com/AlexanderFSP/skb-semantic-release-experiments/commit/4d42e48dad849c2f663fba404425930710b54c60))
