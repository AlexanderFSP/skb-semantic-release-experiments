module.exports = {
  branches: ["dev"],
  plugins: [
    /**
     * By default, only commits with "feat", "fix" or "perf" type will trigger bump
     *
     * @see https://github.com/semantic-release/commit-analyzer/blob/master/lib/default-release-rules.js
     */
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/changelog",
    "@semantic-release/gitlab",
    "@semantic-release/npm",
    "@semantic-release/git",
  ],
};
